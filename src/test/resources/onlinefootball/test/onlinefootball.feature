#Author: asia.morgas@gmail.com
#language: en


Feature: Online Football Betting
		As a WH Customer 
		I want the ability to place a bet on a English Premier League event

@done
Scenario: Online Football Betting
 	
	Given I am on http://sports.williamhill.com/betting/en-gb page
	When I go to Football link
	And I Select odds
	And I Place 0.5 for the home team to �Win�
	Then I get return equal to (0.5 * odds + 0.5)


Scenario Outline: Online Football Betting - Correct values
Given I am on http://sports.williamhill.com/betting/en-gb page
When I go to Football link
And I Select <odds>
And I Place <stake> for the home team to �Win�
Then I get return equal to <result>

Examples:
    | odds  |stake | result |
    |       |      |        |
    |       |      |        | 
