package onlinefootball.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
	


	public class Page {
		
		private String odds;
		private float parsedOdds;
		
		WebDriver driver = new ChromeDriver();
		
		public void landOnPage() {
			driver.get(Locator.url);
		}
		
		public void navigateToFootball() {
			driver.findElement(By.xpath(Locator.footbalLink)).click();

		}

		public void selectOdds() {
			this.odds = driver.findElement(By.xpath(Locator.oddsButton)).getText();
			this.parsedOdds = Float.parseFloat(odds);
			driver.findElement(By.xpath(Locator.oddsButton)).click();

		}
		
		public void placeBet() {
			driver.findElement(By.xpath(Locator.StakeInput)).click();
			driver.findElement(By.xpath(Locator.StakeInput)).clear();
			driver.findElement(By.xpath(Locator.StakeInput)).sendKeys("0.5");
			
			}
		
		public float toReturn() {
			return (float)(parsedOdds *  0.5 + 0.5);
			
			}
	}


