package onlinefootball.test;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;




public class OnlinefootballStepsDef {
	
	protected WebDriver driver;
	protected Page page = new Page();
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chromedriver","C:\\Eclipse\\selenium-java-3.5.3\\Drivers\\chromedriver_win32.chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@Given("^I am on http://sports.williamhill.com/betting/en-gb page$")
	page.landOnPage();
	
	@When("I go to Football link")
	page.navigateToFootball();
	
	@And("^I Select odds$")
	page.selectOdds();
	
	@And("I Place 0.5 for the home team to �Win�")
	page.placeBet();
	
	@Then("^I get return equal to 0.5 * odds + 0.5$")
	public void ensureReturnIsCorrect() {
		assertEquals(page.toReturn(),Float.parseFloat(driver.findElement(By.xpath(Locator.totalReturn)).getText());
		
	}
	
	@After
	public void tearDown() {
		driver.close();
	}
	
	

}
