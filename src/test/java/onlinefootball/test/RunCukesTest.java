package onlinefootball.test;
import cucumber.api.CucumberOptions;
import cucumber.api.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(options = {"pretty,"
		+"html:target/report/report.html",
		"json:target/report/cucu_json_report.json",
		"junit:target/report/cucumber_junit_report.xml"
		})


public class RunCukesTest {
	
}
